#!/usr/bin/python

def split_parentheses(o_str):
  'get one string, out put the result'
  ret = 0.0
  o_str = o_str.replace(' ', '')
  n_list = o_str.split('(')
  n_list.remove('')
  for n_exp in n_list:
    part1, s_exp = n_exp.split(',')
    part2, part3 = s_exp.split(')')
    ret += ((float(part1) + float(part2)) / 2 * float(part3))
  return ret

f = open('TEST.data', 'r')
data = f.read()
show = split_parentheses(data)
print show

